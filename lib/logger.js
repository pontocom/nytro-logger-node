'use strict';

var os = require("os");
var request = require("request");

var NytroLogger = function(NYTRO_LOGGER_TAG, NYTRO_LOGGER_APP, NYTRO_LOGGER_URL){
  this.NYTRO_LOGGER_URL = NYTRO_LOGGER_URL || 'http://logger.nytro.com.br:5555/logger';
  this.NYTRO_LOGGER_APP = NYTRO_LOGGER_APP || "appName";
  this.NYTRO_LOGGER_TAG = NYTRO_LOGGER_TAG || 'nodejs';
  this.error_types = {
      "E": 'ERROR'
      , "R": 'RECOVERABLE_ERROR'
      , "W": 'WARNING'
      , "P": 'PARSE'
      , "N": 'NOTICE'
      , "S": 'STRICT'
      , "D": 'DEPRECATED'
      , "CE": 'CORE_ERROR'
      , "CW": 'CORE_WARNING'
      , "CMR": 'COMPILE_ERROR'
      , "CMW": 'COMPILE_WARNING'
      , "UE": 'USER_ERROR'
      , "UW": 'USER_WARNING'
      , "UN": 'USER_NOTICE'
      , "UD": 'USER_DEPRECATED'
  };

  this.data = {
    'tag': this.NYTRO_LOGGER_TAG,
    'hostname': os.hostname(),
    'type': this.error_types["W"],
    'language': 'nodejs',
    'application': this.NYTRO_LOGGER_APP,
    'data': {},
    'server': {
        'load_average': os.loadavg(),
        'totalmem': os.totalmem()
    }
  };
}

NytroLogger.prototype.emit = function(){
  request.post({url: this.NYTRO_LOGGER_URL, json: this.data }, function(err, resp, body){
    return null;
  });
};

NytroLogger.prototype.warn = function(data){
  this.data.type = this.error_types['W'];
  this.data.data = data;
  this.emit();
};

NytroLogger.prototype.error = function(data){
  this.data.type = this.error_types['E'];
  this.data.data = data;
  this.emit();
};

NytroLogger.prototype.notice = function(data){
  this.data.type = this.error_types['N'];
  this.data.data = data;
  this.emit();
};

module.exports = NytroLogger
